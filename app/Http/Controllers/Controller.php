<?php

namespace App\Http\Controllers;

use Dropbox\Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\Contact;
use App\RsaData;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Endroid\QrCode\ErrorCorrectionLevel;
use Endroid\QrCode\LabelAlignment;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\Writer\PngWriter;
use Endroid\QrCode\Writer\SvgWriter;
use PhpParser\Node\Expr\Cast\Object_;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Auth;
use Khanamiryan;
use Sentry;
use App\Setting;

class Controller extends BaseController
{
    protected $privatekey = '';
    protected $publickey = '';
    protected $key_size = 384;
    protected $certs_path;
    protected $n;
    protected $name;
    protected $keys_loaded = false;

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $data;

    public function __construct()
    {
        $this->middleware('auth');
        $this->data['_page'] = '404';
        $this->_commonNavigation();
        $this->_adminNavigation();
        $this->certs_path = base_path('certs');
        $this->data['keys_loaded'] = $this->keys_loaded;
        $this->data['config'] = $this->setting();
    }

    protected function _commonNavigation()
    {
        $navigation = [
            'Home' => [
                'icon' => 'dashboard',
                'label' => ['value' => '', 'type' => ''],
                'url' => '',
                'children' => []

            ],
            // End Dashboard Nav
            /*
            'general_settings' => [
                'icon' => 'gear',
                'label' => ['value' => '', 'type' => ''],
                'url' => 'settings',
                'children' => [],
            ],
            */
            // End General Settings Nav
            'embed_message' => [
                'icon' => 'envelope',
                'label' => ['value' => '', 'type' => ''],
                'url' => 'embed',
                'children' => [],
            ],
            'extract_message' => [
                'icon' => 'square',
                'label' => ['value' => '', 'type' => ''],
                'url' => 'extract',
                'children' => [],
            ],
            'contacts' => [
                'icon' => 'group',
                'label' => ['value' => '', 'type' => ''],
                'url' => 'contacts',
                'children' => [],
            ],
            'keys' => [
                'icon' => 'key',
                'label' => ['value' => '', 'type' => ''],
                'url' => 'keys',
                'children' => [],
            ],
        ];
        $this->data['common_navigation'] = $navigation;
        return true;
    }

    private function _adminNavigation()
    {
        $navigation = [
            'Home' => [
                'icon' => 'dashboard',
                'label' => ['value' => '', 'type' => ''],
                'url' => '',
                'children' => []

            ],

            'Site_settings' => [
                'icon' => 'gear',
                'label' => ['value' => '', 'type' => ''],
                'url' => 'settings',
                'children' => []

            ],
            'Account_management' => [
                'icon' => 'lock',
                'label' => ['value' => '', 'type' => ''],
                'url' => 'accounts',
                'children' => []

            ],
            /*
            'Contact_messages' => [
                'icon' => 'user',
                'label' => ['value' => '', 'type' => ''],
                'url' => 'messages',
                'children' => []

            ],
            */

        ];
        $this->data['admin_navigation'] = $navigation;
        return true;
    }

    protected function addToBreadcrumb($title, $link, $icon = null, $active = 0)
    {
        $this->data['breadcrumb'][] = [
            'title' => $title,
            'link' => $link,
            'icon' => $icon,
            'is_active' => $active
        ];
    }


    /*
 * QR Codes
 */

    protected function _generateQrCode($public_key, $n, $name, $path = null)
    {
        $qr = new \stdClass();
        $rsa = new \OpenSSL();
        $qr->publicKey = $rsa->formatPEMTOString($public_key);
        $qr->n = $n;
        $qr->name = $name;
        if ($qr->n and $name and $public_key) {
            $qr_text = json_encode($qr);
            $qr_path = $path ? $path : 'publickeys/';
            if (!is_dir($qr_path)) mkdir($qr_path);
            $qr_file = md5(time()) . '.png';
            $qr_code = new QrCode($qr_text);
            $qr_code->setSize(600);
            $qr_code
                ->setMargin(20)
                ->setEncoding('UTF-8')
                ->setErrorCorrectionLevel(ErrorCorrectionLevel::HIGH)
                ->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0])
                ->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255]);
            $qr_code->writeFile($qr_path . $qr_file);
            $qr_readable = $this->_readQrCode($qr_path . $qr_file);
            $qr_readable ?: unlink($qr_path . $qr_file);
            return $qr_readable ? $qr_file : false;
        }
        return false;
    }

    protected function _readQrCode($file)
    {
        $qrcode = new \QrReader($file);
        $text = $qrcode->text();

        /*
                $libern = new \QRCodeReader\QRCodeReader();
                $text_1 = $libern->decode($file);

                dd(['khanmyran'=>$text,'libern'=>$text_1]);
        */
        return $text;
    }


    /*
     * RSA Keypair Implementation
     */

    protected function _generateKeys($password = NULL, $key_size = 1024)
    {

        $status = false;
        $message = '';
        try {
            $rsa = new \OpenSSL();
            $rsa->setKeySizeAndType($key_size);
            $rsa->generateKeyPair();
            $this->publickey = $rsa->getPublicKey();
            $this->privatekey = $rsa->getPrivateKey();
            $status = true;
            $message = 'Keypair Generated Successfully.';
        } catch (\Exception $e) {
            $message = $e->getMessage();
        } finally {
            return ['message' => $message, 'success' => $status];
        }
    }

    protected function _encrypt($public_key, $message, $format_string_to_pem = false)
    {
        try {
            $rsa = new \OpenSSL();
            if ($format_string_to_pem) {
                $public_key = $rsa->formatStringToPEM($public_key);
            }
            $rsa->loadPublicKey($public_key);
            $rsa->setClearText($message);
            $rsa->encryptText();
            return base64_encode($rsa->getCipher());
        } catch (\Exception $e) {
            return false;
        }

    }

    protected function _decrypt($private_key, $ciphertext)
    {
        try {
            echo $private_key;
            $rsa = new \OpenSSL();
            $rsa->loadPrivateKey($private_key);
            $rsa->setCipher(base64_decode($ciphertext));
            $rsa->decryptCipher();
            return $rsa->getCleartext();
        } catch (\Exception $e) {
            return false;
        }
    }

    protected function _loadKeys($user_id)
    {
        if ($user_id) {
            $config = RsaData::where('user_id', $user_id)->first();
            if ($config) {
                $this->publickey = $config->public_key;
                $this->privatekey = $config->private_key;
                if ($this->publickey and $this->privatekey) {
                    $this->keys_loaded = true;
                }
            }
        }
        $this->data['keys_loaded'] = $this->keys_loaded;
        return $this->keys_loaded;
    }

    /*
     * Upload Images
     */

    protected function _uploadImages($files, $path = null)
    {
        $images_path = $path ? $path : 'uploads/';
        if (!is_dir($images_path)) mkdir($images_path);
        foreach ($files as $count => $file) {
            if ($file->isValid()) {
                $file_name = md5(time() . $count) . "." . $file->getClientOriginalExtension();
                if ($file->move($images_path, $file_name)) {
                    return $images_path . $file_name;
                }
            }
        }
        return false;

    }


    /*
     * Application settings and config
     */

    private function setting()
    {
        $config = [];
        $settings = Setting::all();
        foreach ($settings as $setting) {
            $config[$setting->key] = $setting->value;
        }
        return (object)$config;
    }

}
