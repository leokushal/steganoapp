<?php

namespace App\Http\Controllers;

use App\RsaData;
use App\Setting;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

final class AdminController extends Controller
{

    public function __construct()
    {
        @parent::__construct();

    }

    public function index()
    {
        $this->_loadKeys(Auth::user()->id);
        $this->data['_page'] = 'index';
        $this->addToBreadcrumb('Image Steganography', '', 'dashboard');
        $this->addToBreadcrumb('Dashboard', '', 'bookmark-o', 1);
        return view('pages.index', $this->data);
    }

    public function settings()
    {
        $this->_loadKeys(Auth::user()->id);
        $this->data['_page'] = 'index';
        $this->addToBreadcrumb('Image Steganography', '', 'dashboard');
        $this->addToBreadcrumb('Settings', '', 'gear', 1);
        return view('pages.settings', $this->data);
    }

    public function postSettings(Request $request)
    {
        $config_keys = ['app_name', 'app_about', 'website', 'web_title', 'phone', 'email'];

        foreach ($config_keys as $key) {
            $check = Setting::where('key', $key)->count();
            if ($check) {
                Setting::where('key', $key)->update(['value' => $request->$key]);
            } else {
                Setting::insert(['key' => $key, 'value' => $request->$key]);
            }
        }

        return back();
    }

    public function accounts()
    {
        $this->_loadKeys(Auth::user()->id);
        $this->data['users'] = User::where('id', '!=', Auth::user()->id)->get();
        foreach ($this->data['users'] as $id => $user) {
            $user_rsa = RsaData::where('user_id', $user->id)->orderBy('id', 'DESC')->first();
            $this->data['users'][$id]->username = ($user_rsa != null) ? $user_rsa->name : 'N/A';
            $this->data['users'][$id]->keys_status = ($user_rsa != null) ? $user_rsa->public_key : NULL;

        }
        $this->data['_page'] = 'index';
        $this->addToBreadcrumb('Image Steganography', '', 'dashboard');
        $this->addToBreadcrumb('Accounts', '', 'group', 1);
        return view('pages.accounts', $this->data);
    }

    public function postSuspend(Request $request, $id)
    {
        $user = User::find($id);
        if ($user) {
            $user->is_suspended = 1;
            $user->save();
        }
        return back();
    }

    public function postActivate(Request $request, $id)
    {
        $user = User::find($id);
        if ($user) {
            $user->is_suspended = 0;
            $user->save();
        }
        return back();
    }

    public function messages()
    {

    }


}
