<?php

namespace App\Http\Controllers;

use Dropbox\Exception;
use Dropbox\WriteMode;
use Illuminate\Http\Request;
use GrahamCampbell\Dropbox\Facades\Dropbox;

class DropboxController extends Controller
{
    private $path = '/Steganography';


    public function __construct()
    {

    }

    public function share(Request $request, $param)
    {
        $param_decoded = base64_decode($param);
        $file_path = 'embedded/' . $param_decoded;
        $email = $request->email;
        if (!filter_var($email)) {
            $message = '<i class="fa fa-info-circle"></i> The email you provided is invalid. Please try again.';
            return redirect('share/' . $param)->with('message', $message)->with('success', 0)->with('message_header', '<i class="fa fa-dropbox"></i> File Sharing Error');
        }
        $upload = $this->_uploadFile($param_decoded);
        return redirect('share/' . $param)->with('message', $upload['message'] ? $upload['message'] : "An error occurred while adding file to dropbox.")->with('success', (bool)$upload['status'])->with('message_header', '<i class="fa fa-dropbox"></i> File Sharing Status');


    }

    private function _uploadFile($file)
    {
        $status = false;
        $message = null;
        try {
            Dropbox::createFolder($this->path);
            if (is_file('embedded/' . $file)) {
                $fh = file_get_contents('embedded/' . $file);
                try {
                    $status = Dropbox::uploadFileFromString($this->path . "/" . $file, WriteMode::add(), $fh);
                    $message = 'File added to dropbox successfully';
                    if ($status) {

                    }
                } catch (Exception $e) {
                    $message = 'File uploading error with message <strong>' . $e->getMessage() . '</strong>';
                }
            } else {
                $message = 'The file you\'re trying to share is either corrupted or doesn\'t exist';
            }
        } catch (Exception $e) {
            $message = 'File uploading error with message <strong>' . $e->getMessage() . '</strong>';
        }
        return [
            'status' => $status,
            'message' => $message
        ];

    }
}
