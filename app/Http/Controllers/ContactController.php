<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
class ContactController extends Controller
{
    public function __construct()
    {
        @parent::__construct();
    }

    public function contacts()
    {
        $this->data['_page'] = 'contacts';
        $this->data['title'] = 'Contacts Management';
        $this->_loadKeys(Auth::user()->id);
        $this->addToBreadcrumb('Image Steganography', '', 'dashboard');
        $this->addToBreadcrumb('Contacts Management', '', 'bookmark-o', 1);
        $this->data['contacts'] = Contact::where('source_id', Auth::user()->id)->get();
        return view('pages.contacts', $this->data);
    }

    public function postContacts(Request $request)
    {
        $message = $success = false;
        $upload = $this->_uploadImages($request->files);
        if ($upload and is_file($upload)) {
            $decode = (array) json_decode($this->_readQrCode($upload));
            if ($decode) {
                //DB::enableQueryLog();
                $test_contact = Contact::where('source_id', Auth::user()->id)->where('name', $decode['name'])->where('public_key','like', '%'.$decode['publicKey'].'%')->get();
                //dd(DB::getQueryLog());
                extract($decode);
                if (isset($publicKey) and isset($n) and isset($name)) {
                    $success = true;

                    //dd($decode, $test_contact);

                    $n = $n ? base64_decode($n) : 'N/A';
                    $message = "<strong>Contact Name : </strong>$name" . PHP_EOL;
                    $message .= "<strong>Message : </strong>$n" . PHP_EOL;
                    $message .= "<strong>Public Key : </strong>$publicKey" . PHP_EOL;
                    $message = nl2br($message);
                    if ($test_contact->count()) {
                        $message = '<i class="fa fa-info-circle"></i> The contact already exists in your contact-list.</br>' . $message;
                    } else {
                        $rsa = new \OpenSSL();
                        $publicKey = $rsa->formatStringToPEM($publicKey);
                        $contact = new Contact();
                        $contact->source_id = Auth::user()->id;
                        $contact->name = $name;
                        $contact->public_key = $publicKey;
                        $contact->save();
                        $message = '<i class="fa fa-info-circle"></i> New contact added to your contact-list.</br>' . $message;
                    }
                } else {
                    $message = '<i class="fa fa-info-circle"></i> The file contains data in invalid format.';
                }

            } else {
                $message = '<i class="fa fa-info-circle"></i> Either the image or is corrupted or the file is invalid.';
            }
            unlink($upload);
        } else {
            $message = '<i class="fa fa-info-circle"></i> Error occurred while uploading file. Please try again.';
        }
        return redirect('contacts')->with('success', $success)->with('message', $message)->with('message_header', '===== QR SCAN RESULT ===');

    }

}
