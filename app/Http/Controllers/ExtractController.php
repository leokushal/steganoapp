<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Contact;
use Picamator\SteganographyKit\StegoContainer;

class ExtractController extends HomeController
{
    public function __construct()
    {
        @parent::__construct();
    }

    public function extract()
    {
        $this->_loadKeys(Auth::user()->id);
        $this->data['_page'] = 'extract';
        $this->data['title'] = 'Extract Message';
        $this->addToBreadcrumb('Image Steganography', '', 'dashboard');
        $this->addToBreadcrumb('Extract Message', '', 'bookmark-o', 1);
        return view('pages.extract', $this->data);
    }

    public function postExtract(Request $request)
    {
        $this->_loadKeys(Auth::user()->id);
        $file = $this->_uploadImages($request->files, 'uploads/');
        $success = $message = false;
        $decode = $this->_steganoAlgorithm($file);
        if ($decode['success']) {
            $crypt = $decode['message'];
            $decrypted = $this->_decrypt($this->privatekey, $crypt);
            if (null === $decrypted)
                $message = 'You\'re not the verified recipient of the message.';
            elseif (false === $decrypted)
                $message = 'Your public/private keys information aren\'t valid. ';
            elseif ($decrypted) {
                $success = true;
                $message = '<strong>Decrypted Secret Message</strong>' . PHP_EOL;
                $message .= '-------------------------------------' . PHP_EOL;
                $message .= '<p>' . $decrypted . '</p>';
                $message = nl2br($message);
            }
        } else $message = $decode['message'];
        return back()->with('success', $success)->with('message', $message)->with('message_header', 'STEGANOGRAPHY EXTRACT RESULT');
    }

    private function _steganoAlgorithm($file)
    {
        $status = false;
        try {
            if (!is_file($file))
                throw new \Exception('An error occurred while retrieving file information. Make sure the file exists.');

            $stego = new StegoContainer();
            $message = $stego->decode($file);
            $status = true;

        } catch (\Exception $e) {
            $message = $e->getMessage();
        }
        return ['success' => $status, 'message' => $message];
    }


}
