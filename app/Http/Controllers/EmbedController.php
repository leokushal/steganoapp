<?php

namespace App\Http\Controllers;

use Dropbox\Exception;
use Illuminate\Http\Request;
use App\Contact;
use Illuminate\Support\Facades\Auth;
use Picamator\SteganographyKit\StegoContainer;

class EmbedController extends HomeController
{
    function __construct()
    {
        parent::__construct();
    }

    public function embed(Request $request)
    {
        $this->_loadKeys(Auth::user()->id);
        if ($request->contact) {
            $contact_id = (int)base64_decode(urldecode($request->contact));
            if ($contact_id) $this->data['send_to'] = $contact_id;
        }
        $this->data['contacts'] = Contact::where('source_id', Auth::user()->id)->get();
        $this->data['_page'] = 'embed';
        $this->addToBreadcrumb('Image Steganography', '', 'dashboard');
        $this->addToBreadcrumb('Embed Message', '', 'bookmark-o', 1);
        $this->data['title'] = 'Embed Message';

        return view('pages.embed', $this->data);
    }

    public function postEmbed(Request $request)
    {
        $message_string = $request->message;
        $contact = $request->contact;
        $file = $this->_uploadImages($request->files, 'uploads/');
        $success = $message = false;
        if (is_file($file)) {
            $receiver = Contact::where('id', $contact)->where('source_id', Auth::user()->id)->first();
            if ($receiver->count()) {
                $rsa = new \OpenSSL();
                $embed = new \stdClass();
                $embed->publicKey = $receiver->public_key;
                $embed->name = $contact;
                $encrypted = $this->_encrypt($receiver->public_key, $message_string);
                if ($encrypted) {
                    $stegano = $this->_steganoAlgorithm($file, $encrypted);
                    if ($stegano['success']) {
                        $success = true;
                        $embeded_file = $stegano['file'];
                    } else {
                        $message = $stegano['message'];
                    }
                } else {
                    $message = 'Stored Keys information for the contact is invalid.';
                }

            } else {
                $message = '<i class="fa fa-info-circle"></i> The contact information about the receiver doesnot exist in your list.';
            }

        } else {
            $message = '<i class="fa fa-info-circle"></i> An error occurred while retrieving file information. Please try again.';
        }
        return $success
            ? redirect('share/' . base64_encode($embeded_file))->with('image', base64_encode($embeded_file))
            : back()->with('success', $success)->with('message', $message)->with('message_header', '===== QR EMBED ERROR =====');
    }

    private function _steganoAlgorithm($file, $message)
    {
        $return = ['success' => false, 'message' => null, 'file' => null];
        if (!$file or !is_file($file) or !$message) {
            $return['message'] = 'The file or message provided for steganography doesn\'t exist.';
            return $return;
        }
        try {
            $file_tmp = explode('.', $file);
            $file_ext = end($file_tmp);
            if (!in_array(strtolower($file_ext), ['png', 'jpg', 'jpeg']))
                throw new \Exception('The image type is not supported. Please use PNG or JPEG file.');
            $container = 'embedded/';
            if (!is_dir($container)) mkdir($container);
            $output_file = md5(time()) . "." . $file_ext;
            $output_path = $container . $output_file;
            $stego = new StegoContainer();
            $stego->encode($file, $output_path, $message);
            $return['success'] = true;
            $return['message'] = 'Message has been embedded successfully.';
            $return['file'] = $output_file;

        } catch (\Exception $e) {
            $return['message'] = $e->getMessage();
        }
        return $return;
    }

}
