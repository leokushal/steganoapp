<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function __construct()
    {
        @parent::__construct();
    }


    public function index()
    {
        $this->_loadKeys(Auth::user()->id);
        $this->data['_page'] = 'index';
        $this->addToBreadcrumb('Image Steganography', '', 'dashboard');
        $this->addToBreadcrumb('Dashboard', '', 'bookmark-o', 1);
        if (Auth::user()->is_admin) {
            $this->data['users_total'] = User::count();
            $this->data['users_suspended'] = User::where('is_suspended',true)->count();
            $this->data['users_active'] = User::where('is_suspended', false)->count();
            return view('pages.index_su', $this->data);

        } else {
            return view('pages.index', $this->data);
        }
    }
}
