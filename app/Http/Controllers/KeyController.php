<?php

namespace App\Http\Controllers;

use App\RsaData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Contact;

class KeyController extends Controller
{
    public function __construct()
    {
        @parent::__construct();
    }

    public function keys()
    {
        $this->_loadKeys(Auth::user()->id);
        $this->data['_page'] = 'keys';
        $this->data['keys_detail'] = RsaData::where('user_id', Auth::user()->id)->first();
        $this->addToBreadcrumb('Image Steganography', '', 'dashboard');
        $this->addToBreadcrumb('Keys Manager', '', 'bookmark-o', 1);
        return view('pages.keys', $this->data);
    }

    public function postKeys(Request $request)
    {
        $user_id = Auth::user()->id;
        $keys = RsaData::find(['user_id' => $user_id])->first();
        if (!$keys or !$keys->count()) {
            $keys = new RsaData();
            $keys->user_id = $user_id;
        }
        $generate = $this->_generateKeys($request->name, $this->key_size);
        if ($generate['success']) {
            $encrypted = $this->_encrypt($this->publickey, 'HELLO');
            if ($encrypted) {
                $image = $this->_generateQrCode($this->publickey, base64_encode('HELLO'), $request->name, 'publickeys/');
                if ($image) {
                    $keys->public_key = $this->publickey;
                    $keys->private_key = $this->privatekey;
                    $keys->name = $request->name;
                    $keys->image = $image ? $image : NULL;
                    $keys->password = '';
                    $keys->save();
                    $this->_loadKeys(Auth::user()->id);
                } else {
                    $generate['message'] = 'IMPORTANT!! A readable file cannot be generate at the moment. Please try again.';
                    $generate['success'] = false;
                }
            } else {
                $generate['message'] = 'Keypair generation error. Please check openssl configuration file.';
                $generate['success'] = false;
            }
        }
        return back()->with([
            'message' => $generate['message'],
            'success' => $generate['success'],
            'message_header' => 'Key Pair Generating Status'
        ]);
    }
}
