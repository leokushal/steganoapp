<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;


class GoogleController extends Controller
{
    private $client, $service;

    public function oAuth(Request $request)
    {
        $this->_initDrive();
        $this->client->fetchAccessTokenWithAuthCode($request->code);
        $access_file = $request->session()->get('access_file');
        if (!$access_file) return redirect('share/' . $access_file)->with('success', 0)->with('message', 'The file you\'re trying to share with google drive doesn\'t exist.');
        $access_token = $this->client->getAccessToken();
        if (!is_array($access_token)) return redirect('share/' . $access_file)->with('success', 0)->with('message', 'An error occurred while authorizing with google. Please try again later.');
        $request->session()->put('access_token', $access_token);
        return redirect('share/drive/' . $access_file);
    }

    public function share(Request $request, $param)
    {
        $param_decoded = base64_decode($param);
        $email = $request->email;
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $file = $request->session()->get('image') ? $request->session()->get('image') : $param_decoded;
            $file_path = 'embedded/' . $file;
            if (!$file_path or !is_file($file_path)) {
                $message = $file ? '<i class="fa fa-info-circle"></i> The file you\'re trying to share doesn\'t exist.' : '<i class="fa fa-info-circle"></i> Please embed your message before you can share.';
                return redirect('embed/')->with('message', $message)->with('success', 0)->with('message_header', '<i class="fa fa-google"></i> File Sharing Error');
            }
            $access_token = $request->session()->get('access_token');
            $this->_initDrive();
            if ($access_token) {
                $this->client->setAccessToken($access_token);
                if (!$this->client->isAccessTokenExpired()) {
                    $share = $this->_setPermission($this->_uploadFile($file_path), $email);
                    return redirect('share/' . $param)->with('message', $share['message'])
                        ->with('success', $share['status'])->with('message_header', $share['status'] ? '<i class="fa fa-google"></i> File sharing successful !' : '<i class="fa fa-google"></i> File sharing Error');

                } else {
                    $request->session()->forget('access_token');
                    return Redirect::to($this->_authorize($request, $param));
                }

            } else {
                return Redirect::to($this->_authorize($request, $param));
            }

        } else {
            $message = '<i class="fa fa-info-circle"></i> The email you provided is invalid. Please try again.';
            return redirect('share/' . $param)->with('message', $message)->with('success', 0)->with('message_header', '<i class="fa fa-google"></i> File Sharing Error');
        }


    }

    private function _initDrive()
    {
        $this->client = new \Google_Client();
        $this->client->setAuthConfig(base_path('client_secret.json'));
        $this->client->addScope(\Google_Service_Drive::DRIVE_FILE);
        $this->client->setRedirectUri(url('oauth'));

        $file_path = is_file('D:\xampp\php\curl\bin\curl-ca-bundle.crt') ? 'D:\xampp\php\curl\bin\curl-ca-bundle.crt': 'C:\xampp\php\curl\bin\curl-ca-bundle.crt';
        $this->client->setHttpClient(new \GuzzleHttp\Client(array(
            'verify' => realpath($file_path),
        )));

        $this->service = new \Google_Service_Drive($this->client);
    }

    private function _authorize($request, $param)
    {
        $request->session()->put('access_file', $param);
        $auth_url = $this->client->createAuthUrl();
        $url = filter_var($auth_url, FILTER_SANITIZE_URL);
        return $url;

    }

    private function _createFolder($folder_name = 'Steganography')
    {
        $pageToken = null;
        $folder_id = null;
        do {
            $response = $this->service->files->listFiles(array(
                'q' => "mimeType='application/vnd.google-apps.folder' and name = 'Steganography' and trashed=false",
                'spaces' => 'drive',
                'pageToken' => $pageToken,
                'fields' => 'nextPageToken, files(id, name)',
            ));

            foreach ($response->files as $file) {
                $folder_id = $file->id;
            }
        } while ($pageToken != null);

        if ($folder_id) {
            return $folder_id;
        } else {
            $fileMetadata = new \Google_Service_Drive_DriveFile(array(
                'name' => $folder_name,
                'mimeType' => 'application/vnd.google-apps.folder'));
            $create = $this->service->files->create($fileMetadata, array('fields' => 'id'));
            return $create->id;
        }
    }

    private function _uploadFile($file_path)
    {
        define("UPLOAD_FILE", $file_path);

        $folder_id = $this->_createFolder();

        $file = new \Google_Service_Drive_DriveFile(array(
            'name' => str_replace("/", "_", UPLOAD_FILE),
            'parents' => array($folder_id)
        ));
        $create = $this->service->files->create(
            $file,
            array(
                'data' => file_get_contents(UPLOAD_FILE),
                'mimeType' => 'image/png',
                'uploadType' => 'multipart',
                'fields' => 'id'
            )
        );

        return ($create and $create->id) ? $create->id : null;
    }

    private function _setPermission($file_id, $email)
    {
        $message = null;
        $status = false;
        $fileId = $file_id;
        $this->service->getClient()->setUseBatch(true);
        try {
            $batch = $this->service->createBatch();

            $userPermission = new \Google_Service_Drive_Permission(array(
                'type' => 'user',
                'role' => 'writer',
                'emailAddress' => $email
            ));

            $request = $this->service->permissions->create(
                $fileId, $userPermission, array('fields' => 'id'));
            $batch->add($request, 'user');

            $results = $batch->execute();

            foreach ($results as $result) {
                if ($result instanceof \Google_Service_Exception) {
                    $message = '<i class="fa fa-info-circle"></i> ' . $result->getMessage();
                    $status = false;
                } else {
                    $message = '<i class="fa fa-info-circle"></i> File has been shared with <strong>' . $email . '</strong> successfully.';
                    $status = true;
                }
            }
        } finally {
            $this->service->getClient()->setUseBatch(false);
        }
        return ['status' => $status, 'message' => $message];
    }
}
