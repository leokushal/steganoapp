<?php

namespace App\Http\Controllers;

use App\Mail\MailImage;
use Dropbox\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;


class ShareController extends HomeController
{
    private $embedded_path = 'embedded/', $publickey_path = 'publickeys/';

    public function __construct()
    {
        @parent::__construct();
    }

    public function share(Request $request, $param)
    {
        $this->_loadKeys(Auth::user()->id);
        $file = $request->session()->get('image') ? $request->session()->get('image') : $param;
        $file = base64_decode($file);
        $file_path = $this->embedded_path . $file;
        if (!$file_path or !is_file($file_path)) {
            $message = $file ? 'The file you\'re trying to share doesn\'t exist.' : 'Please embed your message before you can share.';
            return redirect('embed')->with('message', $message)->with('success', 0)->with('message_header', 'File Sharing Error');
        }
        $this->data['file'] = $param;
        $this->data['_page'] = 'share';
        $this->data['title'] = 'Share Stegna Image';
        $this->addToBreadcrumb('Image Steganography', '', 'dashboard');
        $this->addToBreadcrumb('Share Message', '', 'bookmark-o', 1);
        return view('pages.share', $this->data);
    }

    public function download(Request $request, $param)
    {
        $file = base64_decode($param);
        $file_path = public_path($this->embedded_path . $file);
        return is_file($file_path)
            ? response()->download($file_path)
            : back()->with('message', 'Requested file is not available to download.')->with('success', 0)->with('message_header', 'File not available');

    }

    public function email(Request $request, $param)
    {
        $file = base64_decode($param);
        $email = $request->email;
        $file_path = public_path($this->embedded_path . $file);
        $message = '';
        if (is_file($file_path)) {
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $success = false;
                $data = [
                    'from' => env('MAIL_USERNAME'),
                    'to' => $email,
                    'subject' => 'Steganographic Image',
                    'file' => $file_path,
                ];
                $data = (object)$data;
                $image = new Collection($data);
                try {
                    Mail::to($request->user())->send(new MailImage($image));
                    $message = 'Mail sent to ' . $email . ' successfully.';
                } catch (Exception $e) {
                    $message = 'Sending mail returned error with message ' . $e->getMessage();
                }
                return redirect('share/' . $param)->with('message', $message)->with('success', $success)->with('message_header', $success ? 'Mail Sent Successfully.' : 'Error while sending E-mail');

            } else {
                return redirect('share/' . $param)->with('message', 'The E-mail address you provided is not valid.')->with('success', 0)->with('message_header', 'Invalid E-mail address');
            }
        } else {
            return redirect('share/' . $param)->with('message', 'The requested file is not found. Please try again.')->with('success', 0)->with('message_header', 'File Not Found');
        }

    }

}
