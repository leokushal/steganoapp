<?php

namespace App\Mail;

use Dropbox\Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Collection;

class MailImage extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    protected $image;

    public function __construct(Collection $image)
    {
        $this->image = $image;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from($this->image['from'])
            ->subject($this->image['subject'])
            ->attach($this->image['file'], [
                'as' => 'EMBEDDED_QR_IMAGE.png',
                'mime' => 'image/png',
            ])
            ->text('email.template');
    }
}
