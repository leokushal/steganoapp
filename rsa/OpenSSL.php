<?php

/**
 * Created by PhpStorm.
 * User: arjun
 * Date: 5/30/2017
 * Time: 9:53 AM
 */
class OpenSSL
{

    private $key_size, $key_type, $resource, $public_key, $private_key, $clear_text, $cipher, $encrypted, $decrypted;

    public function __construct()
    {

    }

    public function setKeySizeAndType($size = 1024, $type = OPENSSL_KEYTYPE_RSA)
    {
        if ((int)$size) $this->key_size = $size > 384 ? $size : 384;
        if (defined($type)) $this->key_type = $type;
    }

    public function generateKeyPair($win_path_for_openssl_conf = 'openssl.cnf')
    {
        if (!$this->key_size and !$this->key_type) $this->setKeySizeAndType();
        $conf_openssl = array(
            "private_key_bits" => $this->key_size,
            "private_key_type" => $this->key_type,
        );
        $win_path_for_openssl_conf = base_path($win_path_for_openssl_conf);

        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            if (is_file($win_path_for_openssl_conf))
                $conf_openssl["config"] = $win_path_for_openssl_conf;
            else
                throw new Exception('Cannot find openssl configuration file at "' . $win_path_for_openssl_conf . '". Please make sure the file exists.');
        }

        $exc_message = 'Cannot create key pair. Please check your OpenSSL Settings. [Error : %s]';
        $this->resource = openssl_pkey_new($conf_openssl);
        if ($this->resource) {
            openssl_pkey_export($this->resource, $private);
            //$this->_opensslError();
            //dd($private);
            if (!$private)
                throw new Exception(sprintf($exc_message, openssl_error_string()));
            $key_detail = openssl_pkey_get_details($this->resource);
            $public = $key_detail['key'];
            openssl_free_key($this->resource);
            if (str_contains($public, 'PUBLIC') and str_contains($private, 'PRIVATE')) {
                $this->public_key = $public;
                $this->private_key = $private;
                return true;
            }
        } else {
            throw new Exception(sprintf($exc_message, openssl_error_string()));
        }
    }

    private function _opensslError()
    {
        echo '<pre>';
        while ($e = openssl_error_string()) {
            print_r($e . "\n");
        }
        echo '</pre>';
    }

    public function getPublicKey()
    {
        return $this->public_key;
    }

    public function getPrivateKey()
    {
        return $this->private_key;
    }

    public function loadKeyPair($public, $private)
    {
        $this->loadPublicKey($public);
        $this->loadPrivateKey($private);
    }

    public function loadPublicKey($key)
    {
        $this->public_key = $key;
    }

    public function loadPrivateKey($key)
    {
        $this->private_key = $key;
    }

    public function formatPEMTOString($PEM)
    {
        $trim = [
            '-----BEGIN PUBLIC KEY-----',
            '-----BEGIN PRIVATE KEY-----',
            '-----END PUBLIC KEY-----',
            '-----END PRIVATE KEY-----'
        ];
        $string = trim(str_replace($trim, '', $PEM));
        return str_replace(PHP_EOL, ":", $string);

    }

    public function formatStringToPEM($string, $type = 'public')
    {
        if ($type == 'public') {
            return '-----BEGIN PUBLIC KEY-----' . PHP_EOL . str_replace(':', PHP_EOL, trim($string)) . PHP_EOL . '-----END PUBLIC KEY-----';
        } elseif ($type = 'private') {
            return '-----BEGIN PRIVATE KEY----- ' . PHP_EOL . str_replace(':', PHP_EOL, trim($string)) . PHP_EOL . ' -----END PRIVATE KEY-----';
        }
        return false;
    }

    public function setClearText($text)
    {
        $this->clear_text = $text;
    }

    public function setCipher($cipher)
    {
        $this->cipher = $cipher;
    }

    public function encryptText()
    {
        if ($this->public_key)
            return openssl_public_encrypt($this->clear_text, $this->cipher, $this->public_key);
        return false;
    }

    public function decryptCipher()
    {
        if ($this->private_key)
            return openssl_private_decrypt($this->cipher, $this->clear_text, $this->private_key);
        return false;
    }

    public function getCipher()
    {
        return $this->cipher;
    }

    public function getCleartext()
    {
        return $this->clear_text;
    }
}