<?php

/**
 * Created by PhpStorm.
 * User: arjun
 * Date: 5/30/2017
 * Time: 9:52 PM
 */
use Google\appengine\api;

class GoogleDrive
{
    public function authenticate()
    {
        $client = new Google_Client();
        $client->setAuthConfig(base_path('client_secret.json'));
        $client->setAccessType("offline");        // offline access
        $client->setIncludeGrantedScopes(true);   // incremental auth
        $client->addScope(Google_Service_Drive::DRIVE_METADATA_READONLY);
        $client->setRedirectUri('http://' . $_SERVER['HTTP_HOST'] . '/oauth');
        $auth_url = $client->createAuthUrl();
        return filter_var($auth_url, FILTER_SANITIZE_URL);
    }
}




