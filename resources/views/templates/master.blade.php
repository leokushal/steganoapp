<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  @section('essentials')
    @include('layouts.essentials')
    @show
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  @section('header')
    @include('layouts.header')
  @show
  <!-- Left side column. contains the logo and sidebar -->
  @section('navigation')
    @include('layouts.navigation')
  @show


  <!-- Content Wrapper. Contains page content -->
  @section('content')

  @show

  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  @section('footer')
    @include('layouts.footer')
  @show

</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->
@section('scripts')
  @include('layouts.scripts')
@show

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. Slimscroll is required when using the
     fixed layout. -->
</body>
</html>
