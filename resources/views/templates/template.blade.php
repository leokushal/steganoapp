@extends('templates.master)

@section('essentials')
    @parent
@endsection

@section('header')
    @parent
@endsection

@section('navigation')
    @parent
@endsection

@section('content')

@endsection

@section('footer')
    @parent
@endsection

@section('scripts')
    @parent
@endsection