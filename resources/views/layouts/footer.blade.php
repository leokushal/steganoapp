<footer class="main-footer">
    <!-- Default to the left -->
    <strong>Copyright &copy; {{ date('Y') }} <a href="{{ url('') }}">Stega Image</a>.</strong> All rights reserved.
</footer>
