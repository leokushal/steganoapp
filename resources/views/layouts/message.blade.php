@if(Session::get('message'))
    <div class="col-sm-12">
        <div class="box box-{{ Session::get('success') ? 'success' : 'warning' }} box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">{!! Session::get('message_header') ? Session::get('message_header') : 'QR Scan Results' !!}</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                class="fa fa-times"></i></button>
                </div>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                {!! Session::get('message') !!}
            </div>
            <!-- /.box-body -->
        </div>
    </div>
    <div class="clearfix"></div>
@endif
