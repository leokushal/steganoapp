<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('images/logo_sm.png') }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->fname }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">Mountain Paradise</li>

            @if(Auth::user()->is_admin)
                <?php
                //$navigation = array_merge($admin_navigation, $common_navigation) ;
                $navigation = $admin_navigation;
                ?>
            @else
                <?php
                $navigation = $common_navigation;
                ?>
            @endif


            @foreach($navigation as $nav=>$items)
                @if(empty($items['children']))
                    <li @if($nav==@$page) class="active" @endif ><a href="{{ url($items['url']) }}"><i
                                    class="fa fa-{{ $items['icon'] }}"></i>
                            <span>{{ ucwords(str_replace('_',' ',$nav)) }}</span></a></li>
                @else
                    <li class="treeview
                    @foreach($items['children'] as $sub=>$det)
                    @if($nav.".".$sub == @$page) active @endif
                    @endforeach
                            ">
                        <a href="#">
                            <i class="fa fa-{{ $items['icon'] }}"></i>
                            <span>{{ ucwords(str_replace('_',' ',$nav)) }}</span>
                            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                        </a>
                        <ul class="treeview-menu
                        @foreach($items['children'] as $sub=>$det)
                        @if($nav.".".$sub == @$page) menu-open @endif
                        @endforeach
                                ">
                            @foreach($items['children'] as $sub=>$det)
                                <li @if($nav.".".$sub == @$page) class="active" @endif>
                                    <a href="{{ url($det['url']) }}"><i
                                                class="fa fa-{{ $det['icon'] }}"></i> {{ ucwords(str_replace('_',' ',$sub)) }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                @endif
            @endforeach

        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
