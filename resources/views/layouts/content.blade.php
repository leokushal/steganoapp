<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {{ $page_header or 'Image Steganography' }}
            <small>{{ $page_description or 'Administration Area' }}</small>
        </h1>
        <ol class="breadcrumb">
            @if(!empty($breadcrumb))
                @foreach($breadcrumb as $bc)
                    <li {{ $bc['is_active'] ? ' class="active"' : '' }}>
                        @if(!is_null($bc['link']))
                            <a href="{{ url('admin/'.$bc['link']) }}"><i class="fa fa-{{ $bc['icon'] }}"></i> {{ $bc['title'] }}</a>
                        @else
                            {{ $bc['title'] }}
                        @endif
                    </li>
                @endforeach
            @else
                <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i>Mountain Paradise</a></li>
                <li class="active">Admin</li>
            @endif
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        @if(isset($page) and is_file(base_path('resources/views/admin/pages/'.str_replace('.','/',$page).'.blade.php')))
            @include('admin.pages.'.$page)
        @else
            @include('admin.errors.404')
        @endif
    </section>
    <!-- /.content -->
</div>
