<section class="content-header">
    <h1>
        {{ $page_header or 'Image Steganography' }}
        <small>{{ $page_description or 'User Dashboard' }}</small>
    </h1>
    <ol class="breadcrumb">
        @if(!empty($breadcrumb))
            @foreach($breadcrumb as $bc)
                <li {{ $bc['is_active'] ? ' class="active text-muted"' : '' }}>
                    @if(!$bc['is_active'] and !is_null($bc['link']))
                        <a href="{{ url($bc['link']) }}"><i
                                    class="fa fa-{{ $bc['icon'] }}"></i> {{ $bc['title'] }}</a>
                    @else
                        <i class="fa fa-{{ $bc['icon'] }}"></i> {{ $bc['title'] }}
                    @endif
                </li>
            @endforeach
        @else
            <li><a href="{{ url('') }}"><i class="fa fa-dashboard"></i>Stega Image</a></li>
            <li class="active">Dashboard</li>
        @endif
    </ol>
</section>
@if(!$keys_loaded and !Auth::user()->is_admin)
    <section class="key-error" style="padding: 10px 10px 0 10px;">
        <div class="alert alert-danger alert-dismissable" style="margin: 0">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Keys Error!</h4>
            You haven't generated your keys. Please generate new keys for application from <a href="{{ url('keys') }}">Here</a>.
        </div>
    </section>
@endif