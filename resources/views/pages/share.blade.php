@extends('templates.master')

@section('essentials')
    @parent

@endsection
@section('title')
    {{ $title or 'Welcome' }} : Image Steganography
@endsection

@section('header')
    @parent
@endsection

@section('navigation')
    @parent
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
    @include('layouts.page_header')
    <!-- Main content -->
        <section class="content">
            <div class="box box-default">
                <div class="box-header">
                    <h3 class="box-title">Share Embedded QR Image</h3>
                </div>
                <div class="box-body">
                    @include('layouts.message')
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box bg-gray">
                            <span class="info-box-icon bg-aqua"><i class="fa fa-envelope-o"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">E-Mail</span><br>
                                <span class="info-box-number"><a class="btn btn-sm btn-primary prompt-email"
                                                                 href="{{ url('email/'.$file) }}">Click Here</a></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box bg-gray">
                            <span class="info-box-icon bg-green"><i class="fa fa-download"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Download</span><br>
                                <span class="info-box-number"><a class="btn btn-sm btn-primary"
                                                                 href="{{ url('download/'.$file) }}">Click Here</a></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box bg-gray">
                            <span class="info-box-icon bg-yellow"><i class="fa fa-google"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Google Drive</span><br>
                                <span class="info-box-number"><a class="btn btn-sm btn-primary prompt-email"
                                                                 href="{{ url('share/drive/'.$file) }}">Share</a></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box bg-gray">
                            <span class="info-box-icon bg-red"><i class="fa fa-dropbox"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Dropbox</span><br>
                                <span class="info-box-number"><a class="btn btn-sm btn-primary prompt-email"
                                                                 href="{{ url('share/dropbox/'.$file) }}">Share</a></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->

                </div>
                <div class="box-footer">

                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('footer')
    @parent
@endsection

@section('scripts')
    @parent
    <script type="text/javascript" src="https://apis.google.com/js/api.js"></script>
    <script type="text/javascript">
        init = function () {
            s = new gapi.drive.share.ShareClient();
            s.setOAuthToken('{{Session::get('access_token')? Session::get('access_token')['access_token']:'' }}');
            s.setItemIds(['<FILE_ID>']);
        }
        window.onload = function () {
            gapi.load('drive-share', init);
        }
        $('.prompt-email').click(function (e) {
            e.preventDefault();
            var email = prompt('Please enter E-mail Address to share')

            if (validateEmail(email)) {
                window.location = $(e.target).attr('href')+'?email='+encodeURI(email);
            } else {
                alert('Invalid E-mail. Please try again')
            }
        })

        function validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }

    </script>
@endsection

