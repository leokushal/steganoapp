@extends('templates.master')

@section('essentials')
    @parent
@endsection
@section('title')
    {{ $title or 'Welcome' }} : Image Steganography
@endsection

@section('header')
    @parent
@endsection

@section('navigation')
    @parent
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
    @include('layouts.page_header')
    <!-- Main content -->
        <section class="content">
            <form action="" method="post" class="form-horizontal">

                <div class="box box-primary">
                    <div class="box-header">
                        <div class="box-title">Settings</div>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="app_name">Application Name</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" required name="app_name" value="{{ $config->app_name or ''}}" id="app_name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="app_about">About Application</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" required name="app_about" value="{{ $config->app_about or ''}}" id="app_about">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="website">Website</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" required name="website" value="{{ $config->website or ''}}" id="website">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="web_title">Website Title</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" required name="web_title" value="{{ $config->web_title or ''}}" id="web_title">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="phone">Contact Phone</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" required name="phone" value="{{ $config->phone or ''}}" id="phone">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="email">Contact E-mail</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" required name="email" value="{{ $config->email or ''}}" id="email">
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        {{ csrf_field() }}
                        <input type="submit" name="submit" value="Save" class="btn btn-primary btn-lg pull-right">
                    </div>
                </div>
            </form>

        </section>
        <!-- /.content -->
    </div>
@endsection

@section('footer')
    @parent
@endsection

@section('scripts')
    @parent
@endsection