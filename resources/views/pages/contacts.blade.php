@extends('templates.master')

@section('essentials')
    @parent
    <style>
        .box-actions {
            margin: 20px 0;
        }
    </style>
@endsection
@section('title')
    {{ $title or 'Welcome' }} : Image Steganography
@endsection

@section('header')
    @parent
@endsection

@section('navigation')
    @parent
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
    @include('layouts.page_header')
    <!-- Main content -->
        <section class="content">
            <div class="box box-default">
                <div class="box-header">
                    <h3 class="box-title">Contacts</h3>
                </div>
                <div class="box-body">
                    <div class="col-sm-12 box-actions">
                        <button class="btn btn-primary" id="pick-image" onclick="openBrowse()">Scan QR Image</button>
                        <form action="" method="post" enctype="multipart/form-data" class="">
                            {{csrf_field()}}
                            <input type="file" class="hidden" name="image" id="browse" accept="image/*" capture="camera">
                            <input type="submit" class="hidden" name="submit" id="Scan Now">
                        </form>
                    </div>
                    @include('layouts.message')
                    <div class="col-sm-4">
                        <ul class="list-group">
                            @if(isset($contacts) and $contacts->count())
                                @foreach($contacts as $contact)
                                    <li class="list-group-item">
                                        {{ $contact->name }}
                                        <a href="{!! url('embed?contact='.urlencode(base64_encode($contact->id))) !!}"
                                           class="pull-right"><i
                                                    class="fa fa-envelope"></i></a>
                                    </li>
                                @endforeach
                            @else
                                <li class="list-group-item">
                                    <i class="fa fa-info-circle"></i>
                                    <i>No Contacts Available.</i>
                                </li>

                            @endif
                        </ul>
                    </div>
                </div>
            </div>

        </section>
        <!-- /.content -->
    </div>
@endsection

@section('footer')
    @parent
@endsection

@section('scripts')
    @parent
    <script type="text/javascript">
        function openBrowse() {
            $('#browse').trigger('click');
        }

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $(document).ready(function () {
            $('input[name=image]').on('change', this, function () {
                console.log('ok');
                $('input[type=submit]').trigger('click');
                readURL(this);
            })
        });


    </script>
@endsection