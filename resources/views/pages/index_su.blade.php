@extends('templates.master')

@section('essentials')
    @parent
    <style>
        .dashboard-logo{

        }
        .dashboard-logo img{
            width: 100%;
        }
        .dashboard-display{

        }
    </style>
@endsection
@section('title')
    {{ $title or 'Welcome' }} : Image Steganography
@endsection

@section('header')
    @parent
@endsection

@section('navigation')
    @parent
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
    @include('layouts.page_header')
    <!-- Main content -->
        <section class="content">
            <div class="box box-default">
                <div class="box-body">

                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header bg-yellow">
                            <div class="widget-user-image">
                                <img class="img-circle" src="{{ asset('images/logo_md.png') }}" alt="Steganography">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Welcome to Image Steganography</h3>
                            <h5 class="widget-user-desc">RSA Encrypted secured Messaging System</h5>
                        </div>
                        <div class="box-footer no-padding">
                            <div class="row">
                                <div class="col-lg-3 col-xs-6">
                                    <!-- small box -->
                                    <div class="small-box bg-aqua">
                                        <div class="inner">
                                            <h3>{{ $users_total-1 }}</h3>

                                            <p>Total Users</p>
                                        </div>
                                        <div class="icon">
                                            <i class="fa fa-user"></i>
                                        </div>
                                        <a href="{{ url('accounts') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                                    </div>
                                </div>
                                <!-- ./col -->
                                <div class="col-lg-3 col-xs-6">
                                    <!-- small box -->
                                    <div class="small-box bg-green">
                                        <div class="inner">
                                            <h3>{{ $users_active-1 }}</h3>

                                            <p>Active Users</p>
                                        </div>
                                        <div class="icon">
                                            <i class="fa fa-user"></i>
                                        </div>
                                        <a href="{{ url('accounts') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                                    </div>
                                </div>
                                <!-- ./col -->
                                <div class="col-lg-3 col-xs-6">
                                    <!-- small box -->
                                    <div class="small-box bg-yellow">
                                        <div class="inner">
                                            <h3>{{ $users_suspended }}</h3>

                                            <p>Suspended Users</p>
                                        </div>
                                        <div class="icon">
                                            <i class="fa fa-user"></i>
                                        </div>
                                        <a href="{{ url('accounts') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                                    </div>
                                </div>
                                <!-- ./col -->
                                <div class="col-lg-3 col-xs-6">
                                    <!-- small box -->
                                    <div class="small-box bg-red">
                                        <div class="inner">
                                            <h3>0</h3>

                                            <p>Deactivated Users</p>
                                        </div>
                                        <div class="icon">
                                            <i class="fa fa-user"></i>
                                        </div>
                                        <a href="{{ url('accounts') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                                    </div>
                                </div>
                                <!-- ./col -->
                            </div>

                        </div>
                    </div>
                    <!-- /.widget-user -->

                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('footer')
    @parent
@endsection

@section('scripts')
    @parent
@endsection

