@extends('templates.master')

@section('essentials')
    @parent
    <style>
        .timeline-footer, .button-area {
            clear: both;
            margin: 15px 0;
            border: none !important;
            box-shadow: none;
        }

        .timeline .box {
            width: inherit;
            border: none;
            margin: 0;
            padding: 0;
            background: none;
            height: inherit;
            box-shadow: none;
        }

        .timeline-item {
            border: none;
            box-shadow: none;
            -webkit-box-shadow: none;
            -moz-box-shadow: none;
        }
    </style>

@endsection
@section('title')
    {{ $title or 'Welcome' }} : Image Steganography
@endsection

@section('header')
    @parent
@endsection

@section('navigation')
    @parent
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
    @include('layouts.page_header')
    <!-- Main content -->
        <section class="content">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Extract Message</h3>
                </div>
                <div class="box-body">
                    @include('layouts.message')
                    <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                        <ul class="timeline">
                            <!-- timeline time label -->
                            <li>
                                <i class="fa bg-blue one">1</i>

                                <div class="timeline-item box" id="box-image">
                                    <h3 class="timeline-header">Pick Image</h3>

                                    <div class="timeline-body panel-collapse">
                                        <div class="form-group">
                                            <div class="col-sm-8">
                                                <button type="button" class="btn btn-default btn-lg" id="pick-image"
                                                        onclick="openBrowse()">Pick Image
                                                </button>
                                                <input type="file" id="ip-image" class="hidden" name="image">
                                            </div>
                                            <div class="col-sm-12 button-area">
                                                <button type="button" class="btn btn-primary" id="image"
                                                        onclick="handleChange('image')" disabled="">
                                                    Continue
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="timeline-footer">
                                    </div>
                                </div>
                            </li>
                            <li>
                                <i class="fa bg-blue two">2</i>

                                <div class="timeline-item box" id="box-confirm">
                                    <h3 class="timeline-header">Confirmation</h3>

                                    <div class="timeline-body panel-collapse collapse">
                                        <div class="col-sm-12">
                                            Click to confirm data.
                                        </div>
                                        <div class="col-sm-12 button-area">
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-primary" id="confirm">Confirm Data</button>
                                        </div>

                                    </div>
                                    <div class="timeline-footer">
                                    </div>
                                    <div class="overlay"></div>
                                </div>
                            </li>
                            <li>
                                <i class="fa fa-envelope-o bg-gray"></i>
                            </li>
                        </ul>
                    </form>
                </div>
                <div class="box-footer">

                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('footer')
    @parent
@endsection

@section('scripts')
    @parent
    <script type="text/javascript">
        var boxes = ['image', 'confirm'];
        var overlay = '<div class="overlay"></div>';
        function openBrowse() {
            $('#ip-image').trigger('click');
        }

        $(document).ready(function () {
            $('input[name=image]').on('change', this, function () {
                $('#image').removeAttr('disabled');
            })
        });

        function handleChange(box) {
            $('.timeline-body').addClass('collapse').parent().append(overlay);
            $('#box-confirm .timeline-body').removeClass('collapse').parent().find('.overlay').remove();
        }


    </script>

@endsection