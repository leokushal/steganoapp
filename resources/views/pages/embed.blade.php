@extends('templates.master')

@section('essentials')
    @parent
    <style>
        .timeline-footer, .button-area {
            clear: both;
            margin: 15px 0;
        }

        .timeline .box {
            width: inherit;
            border: none;
            margin: 0;
            padding: 0;
            background: none;
            height: inherit;
            box-shadow: none;
        }

    </style>
@endsection

@section('title')
    {{ $title or 'Welcome' }} : Image Steganography
@endsection

@section('header')
    @parent
@endsection

@section('navigation')
    @parent
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
    @include('layouts.page_header')
    <!-- Main content -->
        <section class="content">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Embed Message</h3>
                </div>
                <div class="box-body">
                    @include('layouts.message')
                    <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                        <ul class="timeline">
                            <!-- timeline time label -->
                            <li>
                                <i class="fa  bg-blue one">1</i>

                                <div class="timeline-item box" id="box-message">
                                    <h3 class="timeline-header">Enter new Message</h3>

                                    <div class="timeline-body panel-collapse">
                                        <div class="form-group">
                                            <label for="ip-message" class="control-label col-sm-2">Write your
                                                Message</label>
                                            <div class="col-sm-8">
                                                <input type="text" id="ip-message" name="message" class="form-control"
                                                       placeholder="Your Message Here.." autocomplete="off">
                                            </div>
                                            <div class="col-sm-12 button-area">
                                                <button type="button" class="btn btn-primary" id="message" disabled=""
                                                        onclick="handleChange('message')">Continue
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="timeline-footer">
                                    </div>
                                </div>
                            </li>
                            <li>
                                <i class="fa bg-blue two">2</i>

                                <div class="timeline-item box" id="box-image">
                                    <h3 class="timeline-header">Pick Image</h3>

                                    <div class="timeline-body panel-collapse collapse">
                                        <div class="form-group">
                                            <div class="col-sm-8">
                                                <button type="button" class="btn btn-default btn-lg" id="pick-image"
                                                        onclick="openBrowse()">Pick Image
                                                </button>
                                                <input type="file" id="ip-image" class="hidden" name="image">
                                            </div>
                                            <div class="col-sm-12 button-area">
                                                <button type="button" class="btn btn-primary" id="image" disabled=""
                                                        onclick="handleChange('image')">Continue
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="timeline-footer">
                                    </div>
                                    <div class="overlay"></div>

                                </div>
                            </li>
                            <li>
                                <i class="fa bg-blue three">3</i>

                                <div class="timeline-item box" id="box-contact">
                                    <h3 class="timeline-header">Add Contact</h3>

                                    <div class="timeline-body panel-collapse collapse">
                                        <div class="form-group">
                                            <label for="ip-contact" class="control-label col-sm-2">Select
                                                Contact</label>
                                            <div class="col-sm-4">
                                                <select name="contact" id="ip-contact" class="form-control">
                                                    <option value="">-- Select Contact --</option>
                                                    @if(isset($contacts))
                                                        @foreach($contacts as $contact)
                                                            <option value="{{$contact->id}}"
                                                            @if(isset($send_to) and $send_to==$contact->id)
                                                                <?php $selected = true?>
                                                                        {{ " selected " }}
                                                                    @endif
                                                            >{{ $contact->name }}</option>
                                                        @endforeach
                                                    @endif

                                                </select>
                                            </div>
                                            <div class="col-sm-12 button-area">
                                                <button type="button" class="btn btn-primary" id="contact"
                                                        @if(!isset($selected) or !$selected ) disabled="" @endif
                                                        onclick="handleChange('contact')">Continue
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="timeline-footer">
                                    </div>
                                    <div class="overlay"></div>

                                </div>
                            </li>
                            <li>
                                <i class="fa bg-blue four">4</i>

                                <div class="timeline-item box" id="box-confirm">
                                    <h3 class="timeline-header">Confirmation</h3>

                                    <div class="timeline-body panel-collapse collapse">
                                        <div class="col-sm-12 button-area">
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-primary" id="confirm">Confirm Data
                                            </button>
                                        </div>

                                    </div>
                                    <div class="timeline-footer">
                                    </div>
                                    <div class="overlay"></div>
                                </div>
                            </li>
                            <li>
                                <i class="fa fa-envelope-o bg-gray"></i>
                            </li>
                        </ul>
                    </form>
                </div>
                <div class="box-footer">

                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('footer')
    @parent
@endsection

@section('scripts')
    @parent
    <?php $boxes = ['message', 'image', 'contact']?>
    <script type="text/javascript">
        var boxes = ['image', 'confirm'];
        var overlay = '<div class="overlay"></div>';
        function openBrowse() {
            $('#ip-image').trigger('click');
        }

        $(document).ready(function () {
            @foreach($boxes as $box)
                @if($box=='message')
                $('input[name={{$box}}]').on('keyup', this, function () {
                var msg = $('input[name={{$box}}]').val();
                if (msg.trim()) {
                    $('#{{$box}}').removeAttr('disabled');
                } else {
                    $('#{{$box}}').attr('disabled', 'disabled');
                }
            });
            @else
            $('[name={{$box}}]').on('change', this, function () {
                if ($('[name={{$box}}]').val()) {
                    $('#{{$box}}').removeAttr('disabled');
                } else {
                    $('#{{$box}}').attr('disabled', 'disabled');

                }
            });
            @endif
            @endforeach
        });


        function handleChange(box) {
            $('.overlay').remove();
            $('.timeline-body').addClass('collapse').parent().append(overlay);
            var nextItem = '';
            switch (box) {
                case 'message' :
                    nextItem = 'image';
                    break;
                case 'image' :
                    nextItem = 'contact';
                    break;
                case 'contact' :
                    nextItem = 'confirm';
                    break;

                default :
                    break;
            }
            if (nextItem) {
                console.log(nextItem);
                $('#box-' + nextItem + ' .timeline-body').removeClass('collapse').parent().find('.overlay').remove();
            }
        }


    </script>

@endsection