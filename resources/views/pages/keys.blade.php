@extends('templates.master')

@section('essentials')
    @parent
    <style>
        .key-generate {
            padding: 10px;
        }

        .qr-container {
            padding: 30px 0;
            text-align: center;
        }

        .qr-container img {
            margin: 0 auto;
            border: 10px solid #d0d0d0;
            max-width: 100%;
        }

        .keys-container {
            border: 1px solid #afafaf;
            padding: 20px 10px;
            border-radius: 10px;
        }

        .key-type {
            font-weight: bold;
        }

        .key-value {
            border: 1px solid #d0d0d0;
            padding: 5px;
            text-align: center;
            margin: 10px;
            overflow-wrap: break-word;
        }
    </style>
@endsection
@section('title')
    {{ $title or 'Welcome' }} : Image Steganography
@endsection

@section('header')
    @parent
@endsection

@section('navigation')
    @parent
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
    @include('layouts.page_header')
    <!-- Main content -->
        <section class="content">
            <div class="box">
                <div class="box-header box-default">
                    <h3 class="box-title">
                        Key Generation
                    </h3>
                </div>
                <div class="box-body">
                    @include('layouts.message')
                    <div class="col-sm-12 bg bg-info key-generate">
                        <form class="form" action="" method="post">
                            <div class="form-group">
                                <div class="col-sm-1"></div>
                                <label for="" class="control-label col-sm-2">Enter Name</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="name"
                                           value="{{ strtolower(str_replace(" ","_",Auth::user()->name)) }}"
                                           placeholder="Enter Name Here.." required>
                                </div>
                                <div class="col-sm-4">
                                    {{ csrf_field() }}
                                    <input type="submit" class="btn btn-primary form-control" value="Generate"
                                           name="submit">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="qr-container col-sm-12">
                        <img src="{{ $keys_detail ? asset('publickeys/'.$keys_detail->image) : ''}}" alt="QR Image"
                             class="qr-image">
                    </div>
                    <div class="keys-container col-sm-12">
                        <div class="key-group">
                            <div class="key-type col-sm-2">Public Key :</div>
                            <div class="col-sm-12">
                                <div class="key-value">
                                    {!! ($keys_detail and $keys_detail->public_key) ? nl2br($keys_detail->public_key) : 'Public Key Not Available. Click Generate on top of the page.'!!}

                                </div>
                            </div>
                        </div>
                        <div class="key-group">
                            <div class="key-type col-sm-2">Private Key :</div>
                            <div class="col-sm-12">
                                <div class="key-value">
                                    {!! ($keys_detail and $keys_detail->private_key) ? nl2br($keys_detail->private_key): 'Private Key Not Available. Click Generate on top of the page.' !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('footer')
    @parent
@endsection

@section('scripts')
    @parent
@endsection