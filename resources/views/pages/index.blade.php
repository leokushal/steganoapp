@extends('templates.master')

@section('essentials')
    @parent
    <style>
        .dashboard-logo{

        }
        .dashboard-logo img{
            width: 100%;
        }
        .dashboard-display{

        }
    </style>
@endsection
@section('title')
    {{ $title or 'Welcome' }} : Image Steganography
@endsection

@section('header')
    @parent
@endsection

@section('navigation')
    @parent
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        @include('layouts.page_header')
        <!-- Main content -->
        <section class="content">
            <div class="box box-default">
                <div class="box-body">

                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header bg-yellow">
                            <div class="widget-user-image">
                                <img class="img-circle" src="{{ asset('images/logo_md.png') }}" alt="Steganography">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Welcome to Image Steganography</h3>
                            <h5 class="widget-user-desc">RSA Encrypted secured Messaging System</h5>
                        </div>
                        <div class="box-footer no-padding">
                            <ul class="nav nav-stacked">
                                <li><a href="{{ url('keys') }}">Generate Your Public and Private Keys to encrypt/decrypt messages.</a></li>
                                <li><a href="{{ url('contacts') }}">Add Contacts to your contact-list by scanning QR Code</a></li>
                                <li><a href="{{ url('embed') }}">Encrypt Messages in QR Code</a></li>
                                <li><a href="{{ url('extract') }}">Decrypt Messages from QR Code</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- /.widget-user -->

                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('footer')
    @parent
@endsection

@section('scripts')
    @parent
@endsection

