@extends('templates.master')

@section('essentials')
    @parent
@endsection
@section('title')
    {{ $title or 'Welcome' }} : Image Steganography
@endsection

@section('header')
    @parent
@endsection

@section('navigation')
    @parent
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
    @include('layouts.page_header')
    <!-- Main content -->
        <section class="content">

            <div class="box box-primary">
                <div class="box-header">
                    <div class="box-title">Accounts</div>
                </div>
                <div class="box-body">

                    <table class="table table-striped">
                        <tr>
                            <th>SN</th>
                            <th>Name</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Joined</th>
                            <th>Keys</th>
                            <th>Status</th>
                            <th>Options</th>

                        </tr>
                        @foreach($users as $count=>$user)
                            <tr>
                                <td>{{ $count+1 }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->username }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->created_at }}</td>
                                <td>{{ $user->keys_status ? 'Found':'Not Found' }}</td>
                                <td>{{ $user->is_suspended ? 'Suspended' : 'Activated' }}</td>
                                <td>
                                    <form action="{{ url('accounts/'.($user->is_suspended?'activate':'suspend').'/'.$user->id) }}"
                                          method="post">
                                        {{ csrf_field() }}
                                        <button type="submit"
                                                class="btn btn-sm btn-{{ $user->is_suspended?'primary':'warning' }}">
                                            {{ $user->is_suspended?'Activate':'Suspend' }}
                                        </button>
                                    </form>
                                </td>
                            </tr>

                        @endforeach
                    </table>

                </div>
                <div class="box-footer">

                </div>
            </div>

        </section>
        <!-- /.content -->
    </div>
@endsection

@section('footer')
    @parent
@endsection

@section('scripts')
    @parent
@endsection