<div class="error 404">
    <h3>
        Error 404 :(
    </h3>

    <h5>
        Sorry, The page you requested cannot be found. Please check again or contact support.
    </h5>
</div>
