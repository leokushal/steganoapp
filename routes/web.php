<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/settings', 'HomeController@settings');

Route::get('/embed', 'EmbedController@embed');
Route::post('/embed', 'EmbedController@postEmbed');

Route::get('/share/{param}', 'ShareController@share');
Route::get('/download/{param}', 'ShareController@download');
Route::get('/email/{param}', 'ShareController@email');

Route::get('/extract', 'ExtractController@extract');
Route::post('/extract', 'ExtractController@postExtract');

Route::get('/contacts', 'ContactController@contacts');
Route::post('/contacts', 'ContactController@postContacts');
Route::get('/contacts/add', 'ContactController@addContact');

Route::get('/keys', 'KeyController@keys');
Route::post('/keys', 'KeyController@postKeys');

// Sharing : Google Drive

Route::get('/oauth', 'GoogleController@oAuth');
Route::get('/google/authenticate', 'GoogleController@authenticate');
Route::get('/share/drive/{param}', 'GoogleController@share');
Route::get('/share/dropbox/{param}', 'DropboxController@share');


//Admin Routes

Route::get('/settings', 'AdminController@settings');
Route::post('/settings', 'AdminController@postSettings');

Route::get('/accounts', 'AdminController@accounts');
Route::post('/accounts/suspend/{id}','AdminController@postSuspend');
Route::post('/accounts/activate/{id}','AdminController@postActivate');
Route::get('/messages', 'AdminController@messages');
